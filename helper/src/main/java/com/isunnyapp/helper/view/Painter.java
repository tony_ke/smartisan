package com.isunnyapp.helper.view;

import android.graphics.Canvas;

/**
 * Created by kexuebiao on 15/7/29.
 */
public interface Painter {
    void onSizeChanged(int w, int h, int oldw, int oldh);

    void onDraw(Canvas canvas);
}
