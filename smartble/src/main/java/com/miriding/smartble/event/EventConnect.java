package com.miriding.smartble.event;

import android.bluetooth.BluetoothGatt;

/**
 * Created by kexuebiao on 16/4/27.
 */
public class EventConnect {
    public BluetoothGatt gatt;
    public boolean connected;

    public EventConnect(BluetoothGatt gatt, boolean connected) {
        this.gatt = gatt;
        this.connected = connected;
    }
}
