package com.miriding.smartble.event;

import android.bluetooth.BluetoothGatt;

/**
 * Created by kexuebiao on 16/4/27.
 */
public class EventServiceDiscoverd {
    public BluetoothGatt gatt;
    public boolean success;

    public EventServiceDiscoverd(BluetoothGatt gatt, boolean success) {
        this.gatt = gatt;
        this.success = success;
    }
}
