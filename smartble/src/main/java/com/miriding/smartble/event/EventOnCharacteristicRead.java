package com.miriding.smartble.event;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;

/**
 * Created by kexuebiao on 16/4/27.
 */
public class EventOnCharacteristicRead {
    public BluetoothGatt gatt;
    public BluetoothGattCharacteristic characteristic;
    public boolean success;

    public EventOnCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, boolean success) {
        this.characteristic = characteristic;
        this.gatt = gatt;
        this.success = success;
    }
}
