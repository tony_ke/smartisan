package com.miriding.smartble.event;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattDescriptor;

/**
 * Created by kexuebiao on 16/4/27.
 */
public class EventOnDescriptorWrite {
    public BluetoothGatt gatt;
    public BluetoothGattDescriptor descriptor;
    public boolean success;

    public EventOnDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, boolean success) {
        this.gatt = gatt;
        this.descriptor = descriptor;
        this.success = success;
    }
}
