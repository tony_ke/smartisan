package com.miriding.smartble.event;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;

/**
 * Created by kexuebiao on 16/4/27.
 */
public class EventOnCharacteristicChanged {
    public BluetoothGatt gatt;
    public BluetoothGattCharacteristic characteristic;

    public EventOnCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        this.characteristic = characteristic;
        this.gatt = gatt;
    }
}
