package com.miriding.smartble.Utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by kexuebiao on 16/4/27.
 */
public class BleTool {
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static long getRec(Long t, Long lastT) {
        return t >= lastT ? (t - lastT) : (t - lastT + 4294967296l);
    }

    public static int getRec(Integer t, Integer lastT) {
        return t >= lastT ? (t - lastT) : (t - lastT + 65536);
    }

    /**
     * bytes转字符串
     *
     * @param bytes
     * @return
     */
    public static String toHexString(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static int convertTwoBytesToInt(byte[] data, int index) {
        return ByteBuffer.wrap(data, index, 2).order(ByteOrder.LITTLE_ENDIAN).getShort() & 0xFFFF;
    }

    public static int convertFourBytesToLong(byte[] data, int index) {
        return ByteBuffer.wrap(data, index, 4).order(ByteOrder.LITTLE_ENDIAN).getShort() & 0xFFFFFFFF;
    }
}
