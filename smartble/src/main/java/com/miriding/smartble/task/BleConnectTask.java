package com.miriding.smartble.task;

import android.bluetooth.BluetoothGatt;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.miriding.smartble.event.EventConnect;
import com.miriding.smartble.event.EventConnectOverTime;
import com.miriding.smartble.event.EventServiceDiscoverd;
import com.miriding.smartble.module.BleBus;
import com.miriding.smartble.module.BleDevice;

import de.greenrobot.event.EventBus;

/**
 * Created by kexuebiao on 16/4/26.
 */
public class BleConnectTask extends BleTask<String> {
    String TAG = "BleConnectTask";
    String action = "";
    Handler handler;
    long connectOverTime = 20 * 1000;
    long serviceOverTime = 20 * 1000;
    BluetoothGatt gatt;
    boolean taskEnable = true;

    public BleConnectTask(BleDevice bleDevice) {
        Log.d(TAG, "BleConnectTask -------------------------" + this.hashCode());
        this.bleDevice = bleDevice;
        EventBus.getDefault().register(this);
        handler = new Handler(Looper.getMainLooper());
    }

    public BleConnectTask setConnectOverTime(long connectOverTime) {
        this.connectOverTime = connectOverTime;
        return this;
    }

    public BleConnectTask setServiceOverTime(long serviceOverTime) {
        this.serviceOverTime = serviceOverTime;
        return this;
    }

    Runnable timeoutRunnable = new Runnable() {
        @Override
        public void run() {
            taskEnable = false;
            Log.e(TAG, "任务超时" + BleConnectTask.this.hashCode());

            //操作超时,结束任务
            onWait = false;
            EventBus.getDefault().post(new EventConnectOverTime(bleDevice));
            try {
                if (gatt != null) {
                    gatt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "gatt close");
            }

        }
    };

    public void onEvent(EventServiceDiscoverd event) {
        if (!taskEnable) {
            Log.e(TAG, "任务已终止");
            return;
        }
        if (event.gatt.getDevice().getAddress().equals(bleDevice.address)) {
            taskEnable = false;
            handler.removeCallbacks(timeoutRunnable);
            onWait = false;
        }
    }

    public void onEvent(EventConnect event) {
        if (!taskEnable) {
            Log.e(TAG, "任务已终止");
            return;
        }

        if (event.gatt.getDevice().getAddress().equals(bleDevice.address)) {
            Log.d(TAG, "EventConnect -------- " + this.hashCode());
            handler.removeCallbacks(timeoutRunnable);
            if (event.connected) {
                //设备已连接,发现服务
                gatt = event.gatt;
                BleBus.single.discoverServices(gatt);
                handler.postDelayed(timeoutRunnable, serviceOverTime);
                Log.d(TAG, action + " 设备已连接,准备发现服务");
            } else {
                //设备已断开,结束任务
                Log.e(TAG, "断开");
                onWait = false;
                taskEnable = false;
            }
        }
    }

    public BleConnectTask setAction(String action) {
        this.action = action;
        return this;
    }

    @Override
    public String call() throws Exception {
        if (!taskEnable) {
            Log.e(TAG, "任务已终止");
            return null;
        }
        Log.i(TAG, "开始执行任务  " + action + "-" + this.hashCode());
        onWait = true;

        gatt = BleBus.single.connectGatt(bleDevice);
        handler.postDelayed(timeoutRunnable, connectOverTime);

        //任务没有终止并处于等待中
        while (onWait && taskEnable) {

        }
        EventBus.getDefault().unregister(this);
        Log.i(TAG, "结束执行任务  " + action + "-" + this.hashCode());

        return "";
    }
}
