package com.miriding.smartble.task;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.miriding.smartble.event.EventConnect;
import com.miriding.smartble.event.EventOnCharacteristicWrite;
import com.miriding.smartble.module.BleBus;
import com.miriding.smartble.module.BleDevice;

import java.util.UUID;

import de.greenrobot.event.EventBus;

/**
 * Created by kexuebiao on 16/4/28.
 */
public class BleWriteTask extends BleTask<String> {
    UUID serviceUUID, chUUID;
    byte[] data;
    Handler handler;
    long overTime = 20 * 1000;
    boolean withResponse;

    public BleWriteTask(BleDevice bleDevice, UUID serviceUUID, UUID chUUID, byte[] data) {
        this(bleDevice, serviceUUID, chUUID, data, false);
    }

    public BleWriteTask(BleDevice bleDevice, UUID serviceUUID, UUID chUUID, byte[] data, boolean withResponse) {
        this.withResponse = withResponse;
        this.data = data;
        this.serviceUUID = serviceUUID;
        this.chUUID = chUUID;
        this.bleDevice = bleDevice;

        TAG = "BleWriteTask";
        Log.e(TAG, TAG + " -------------------------" + this.hashCode());
        EventBus.getDefault().register(this);
        handler = new Handler(Looper.getMainLooper());
    }

    public void onEvent(EventOnCharacteristicWrite event){
        if (!taskEnable) {
            Log.e(TAG, action + " 任务已终止");
            return;
        }

        if (event.gatt.getDevice().getAddress().equals(bleDevice.address)) {
            if (event.characteristic.getUuid().equals(chUUID)) {
                handler.removeCallbacks(timeoutRunnable);
                if (event.success) {
                    //设备已断开,结束任务
                    Log.i(TAG, action + " Wri 成功");
                } else {
                    Log.e(TAG, action + " Notify 失败");
                }
                onWait = false;
                taskEnable = false;
            }
        }
    }

    public void onEvent(EventConnect event) {
        if (!taskEnable) {
            Log.i(TAG, "任务已终止");
            return;
        }

        if (event.gatt.getDevice().getAddress().equals(bleDevice.address)) {
            if (!event.connected) {
                handler.removeCallbacks(timeoutRunnable);
                Log.e(TAG, "EventConnect -------- " + this.hashCode());
                //设备已断开,结束任务
                Log.e(TAG, "断开");
                onWait = false;
                taskEnable = false;
            }
        }
    }

    Runnable timeoutRunnable = new Runnable() {
        @Override
        public void run() {
            taskEnable = false;
            Log.e(TAG, action + "任务超时" + BleWriteTask.this.hashCode());

            //操作超时,结束任务
            onWait = false;
        }
    };

    @Override
    public String call() throws Exception {
        if (!taskEnable) {
            Log.i(TAG, "任务已终止");
            return null;
        }
        Log.i(TAG, "开始执行任务  " + action + "-" + this.hashCode());
        onWait = true;

        BleBus.single.write(bleDevice.gatt, serviceUUID, chUUID, data, true);
        handler.postDelayed(timeoutRunnable, overTime);

        //任务没有终止并处于等待中
        while (onWait && taskEnable) {

        }
        EventBus.getDefault().unregister(this);
        Log.i(TAG, "结束执行任务  " + action + "-" + this.hashCode());

        return "";
    }
}
