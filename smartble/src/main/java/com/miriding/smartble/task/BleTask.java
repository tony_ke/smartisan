package com.miriding.smartble.task;

import com.miriding.smartble.module.BleDevice;

import java.util.concurrent.Callable;

/**
 * Created by kexuebiao on 16/4/26.
 */
public abstract class BleTask<T> implements Callable<T> {
    String TAG = "BleTask";
    String action = "BleTask";
    BleDevice bleDevice;
    boolean taskEnable = true;
    boolean onWait;

    public BleTask setAction(String action) {
        this.action = action;
        return this;
    }
}
