package com.miriding.smartble.module;


import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.miriding.smartble.Utils.BleTool;
import com.miriding.smartble.event.EventOnDescriptorWrite;
import com.miriding.smartble.task.BleNotifyTask;
import com.miriding.smartble.task.BleReadTask;
import com.miriding.smartble.task.BleWriteTask;

/**
 * Created by kexuebiao on 16/4/27.
 */
public class R1 extends BleDevice {
    final String TAG = "R1";
    OnListen mOnListen;
    int wheelPerimeter = 2155;
    long lastWheelCount;
    float lastWheelEventTime;
    float mCurrentGearRatio;
    private final int[] mGearsFront = new int[]{53, 39};
    private final int[] mGearsRear = new int[]{11, 12, 13, 14, 15, 17, 19, 21, 23, 25, 28};

    public R1(String address) {
        super(address);
        this.name = "R1";
    }

    @Override
    public void onServiceDiscoverd() {
        super.onServiceDiscoverd();
        BleBus.single.submitMission(new BleNotifyTask(this, Profile.UUID_SERVICE_POWER, Profile.UUID_C_POWER).setAction("功率"));
        BleBus.single.submitMission(new BleNotifyTask(this, Profile.UUID_SERVICE_BATTERY, Profile.UUID_C_BATTERY).setAction("电池"));
        BleBus.single.submitMission(new BleNotifyTask(this, Profile.UUID_SETTING_SERVICE, Profile.UUID_C_SETTING).setAction("设置"));
        BleBus.single.submitMission(new BleNotifyTask(this, Profile.UUID_SETTING_SERVICE, Profile.UUID_C_DI2_NEW).setAction("电变"));
    }

    @Override
    public void onNotify(EventOnDescriptorWrite event) {
        Log.e(TAG, "notify " + (event.success ? "成功" : "失败"));
        if (!event.success) {
            if (gatt != null && gatt.getService(event.descriptor.getUuid()) != null) {
                //重新监听
                BleBus.single.submitMission(
                        new BleNotifyTask(this, event.descriptor.getUuid(), event.descriptor.getCharacteristic().getUuid()).setAction("电池"));
            }
        }
    }

    @Override
    public void OnCharacteristicChanged(BluetoothGattCharacteristic ch) {
        if (ch == null || ch.getValue() == null || ch.getValue().length == 0) {
            return;
        }
        if (ch.getUuid().equals(Profile.UUID_C_POWER)) {
            //功率
            dealPowerData(ch);
        } else if (ch.getUuid().equals(Profile.UUID_C_BATTERY)) {
            //电池
            dealBatteryData(ch);
        } else if (ch.getUuid().equals(Profile.UUID_C_DI2)) {
            //旧的电变
        } else if (ch.getUuid().equals(Profile.UUID_C_SETTING)) {
            //设置
            if (mOnListen != null) {
                mOnListen.OnAntScanResult(ch.getValue());
            }
        } else if (ch.getUuid().equals(Profile.UUID_C_DI2_NEW)) {
            //电变解析
            dealDi2Data(ch);
        }
    }

    private void dealDi2Data(BluetoothGattCharacteristic ch) {
        byte[] data = ch.getValue();
        int front = data[3];
        int rear = data[4];
        int battery = data[5];
        int power_battery = data[7];

        if (front > 0 && rear > 0) {
            mCurrentGearRatio = (float) (mGearsFront[mGearsFront.length - front])
                    / (float) mGearsRear[mGearsRear.length - rear];
        }
        if (mOnListen != null) {
            mOnListen.OnDi2(front, rear, battery, power_battery);
        }
    }

    private void dealPowerData(BluetoothGattCharacteristic ch) {
        byte[] data = ch.getValue();
        int flag = data[1];

        if (flag != 0x10) {
            return;
        }

        // 瞬时功率 单位是瓦为1的分辨率 强制
        int InstantaneousPower = BleTool.convertTwoBytesToInt(data, 2);
        long CumulativeWheelRevolutions = BleTool.convertFourBytesToLong(data, 4);
        float wheelEventTime = BleTool.convertTwoBytesToInt(data, 8) / 1024f;
        float speed = 0;
        float distance = 0;

        if (lastWheelCount > 0) {
            long wheelCount;
            if (CumulativeWheelRevolutions < lastWheelCount) {
                wheelCount = CumulativeWheelRevolutions + 4294967296l - lastWheelCount;
            } else {
                wheelCount = CumulativeWheelRevolutions - lastWheelCount;
            }
            distance = (wheelCount) * (wheelPerimeter / 1000f);
            if (wheelEventTime != lastWheelEventTime) {
                float seconds;
                if (lastWheelEventTime > wheelEventTime) {
                    seconds = wheelEventTime + 65536 / 1024f - lastWheelEventTime;
                } else {
                    seconds = wheelEventTime - lastWheelEventTime;
                }
                speed = distance / seconds * 3.6f;
            }
        }
        lastWheelEventTime = wheelEventTime;
        lastWheelCount = CumulativeWheelRevolutions;

        float cadence = 0;
        if (InstantaneousPower > 0 && speed > 0 && mCurrentGearRatio != 0) {
            cadence = Math.round((speed / 3.6 * 60) / (wheelPerimeter / 1000) / mCurrentGearRatio);
        }

        if (mOnListen != null) {
            mOnListen.OnPower(InstantaneousPower, speed, cadence, distance);
        }
    }

    @Override
    public void OnConnectStateChange(boolean connected) {
        if (mOnListen != null) {
            mOnListen.OnConnect(connected);
        }
    }

    private void dealBatteryData(BluetoothGattCharacteristic ch) {
        if (mOnListen != null) {
            mOnListen.OnBattery(ch.getValue()[0]);
        }
    }

    public void setOnListen(OnListen mOnListen) {
        this.mOnListen = mOnListen;
    }

    public interface OnListen {
        void OnBattery(int battery);

        void OnDi2(int front, int rear, int battery, int power_battery);

        void OnPower(float power, float speed, float cadence, float distance);

        void OnConnect(boolean connect);

        void OnBleClosed();

        void OnAntScanResult(byte[] data);

        void OnReadAntSetting(byte[] antPowerNumber, byte[] antDi2Number);

        void OnReadProductSN(String number);

        void OnReadBikeSN(String number);
    }


    public void readAntNumber() {
        addBleTask(new BleReadTask(this, Profile.UUID_SETTING_SERVICE, Profile.UUID_READ_ANT_SETTING));
    }

    public void startScanAnt() {
        addBleTask(new BleWriteTask(this, Profile.UUID_SETTING_SERVICE, Profile.UUID_C_SETTING, new byte[]{(byte) 0xFF, 0x00, 0x01}));
    }

    public void writeProductSN(String serialnumber) {
        byte[] data = serialnumber.getBytes();
        addBleTask(new BleWriteTask(this, Profile.UUID_SETTING_SERVICE, Profile.UUID_PRODECT_SERIAL_NUMBER, data));
    }

    public void readProductSN() {
        addBleTask(new BleReadTask(this, Profile.UUID_SETTING_SERVICE, Profile.UUID_PRODECT_SERIAL_NUMBER));
    }

    public void writeAntNumber(byte[] data) {
        addBleTask(new BleWriteTask(this, Profile.UUID_SETTING_SERVICE, Profile.UUID_C_SETTING, data));
    }

    //车架序列号读写
    public void readBikeSN() {
        addBleTask(new BleReadTask(this, Profile.UUID_SETTING_SERVICE, Profile.UUID_BIKE_SERIAL_NUMBER));
    }

    public void writeBikeSN(String serialnumber) {
        byte[] data = serialnumber.getBytes();
        addBleTask(new BleWriteTask(this, Profile.UUID_SETTING_SERVICE, Profile.UUID_BIKE_SERIAL_NUMBER, data));
    }

    public void stopScanAnt() {
        addBleTask(new BleWriteTask(this, Profile.UUID_SETTING_SERVICE, Profile.UUID_C_SETTING, new byte[]{(byte) 0xFF, 0x00, 0x00}));
    }


    @Override
    public void OnConnectStateRead(BluetoothGattCharacteristic ch) {

    }
}
