package com.miriding.smartble.module;

import java.util.UUID;

/**
 * Created by kexuebiao on 15/11/14.
 */
public class Profile {
    public final static UUID UUID_SERVICE_Instrument = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");//
    public final static UUID UUID_C_Instrument = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");//
    public final static UUID UUID_C_Instrument_CP = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");//

    /*
     * 米动BDS传感器
     */
    public final static UUID UUID_SERVICE_BDS = UUID.fromString("46AC8160-71A9-4209-8F73-83B1622EE802");// 小米自行车通讯服务
    public final static UUID UUID_C_BDM = UUID.fromString("46AC8161-71A9-4209-8F73-83B1622EE802");// 小米自行车通讯服务

    /**
     * 功率
     */
    public final static UUID UUID_SERVICE_POWER = UUID.fromString("00001818-0000-1000-8000-00805f9b34fb");// 功率服务
    public final static UUID UUID_C_POWER = UUID.fromString("00002A63-0000-1000-8000-00805f9b34fb");// 功率特征


    /*
     * CSC
     */
    public final static UUID UUID_SERVICE_CSC = UUID.fromString("00001816-0000-1000-8000-00805f9b34fb");// 速度与踏频服务
    public final static UUID UUID_C_CSC = UUID.fromString("00002A5B-0000-1000-8000-00805f9b34fb");// 速度与踏频特征
    public final static UUID UUID_C_CSC_CP = UUID.fromString("00002A55-0000-1000-8000-00805f9b34fb");// 速度与踏频特征

    /*
     * 心率
     */
    public final static UUID UUID_SERVICE_HEARTRATE = UUID.fromString("0000180D-0000-1000-8000-00805f9b34fb");// 心律服务
    public final static UUID UUID_C_HEARTRATE = UUID.fromString("00002A37-0000-1000-8000-00805F9B34FB");// 心率特征
    public final static UUID UUID_CP_HEARTRATE = UUID.fromString("00002A39-0000-1000-8000-00805F9B34FB");// 心率特征
    /*
     * EDS(气压与温度)
     */
    public final static UUID UUID_SERVICE_EDS = UUID.fromString("46AC8140-71A9-4209-8F73-83B1622EE802");// 环境数据服务
    public final static UUID UUID_C_EDS_BAROMETER = UUID.fromString("46AC8141-71A9-4209-8F73-83B1622EE802");// 气压特征
    public final static UUID UUID_C_EDS_THERMOMETER = UUID.fromString("46AC8142-71A9-4209-8F73-83B1622EE802");// 温度特征

    /*
     *G-Sensor
     */
    public final static UUID UUID_SERVICE_Motion = UUID.fromString("0000FC00-0000-1000-8000-00805f9b34fb");// Motion服务
    public final static UUID UUID_C_MOTION = UUID.fromString("0000FC20-0000-1000-8000-00805F9B34FB");// Motion特征

    /*
     * 电池
     */
    public final static UUID UUID_SERVICE_BATTERY = UUID.fromString("0000180F-0000-1000-8000-00805f9b34fb");
    public final static UUID UUID_C_BATTERY = UUID.fromString("00002A19-0000-1000-8000-00805f9b34fb");

    /*
     * DTS 数据传输
     */
    public final static UUID UUID_SERVICE_DTS = UUID.fromString("46AC8150-71A9-4209-8F73-83B1622EE802");
    public final static UUID UUID_C_DTS_CP = UUID.fromString("46AC8151-71A9-4209-8F73-83B1622EE802");
    public final static UUID UUID_C_DTS_PACKET = UUID.fromString("46AC8152-71A9-4209-8F73-83B1622EE802");
    public final static UUID UUID_C_DTS_STATUS = UUID.fromString("46AC8153-71A9-4209-8F73-83B1622EE802");

    /**
     * 设备信息
     */
    public final static UUID UUID_SERVICE_INFP = UUID.fromString("0000180A-0000-1000-8000-00805f9b34fb");
    public final static UUID UUID_C_MANUFACTURER = UUID.fromString("00002A29-0000-1000-8000-00805f9b34fb");
    public final static UUID UUID_C_MODEL = UUID.fromString("00002A24-0000-1000-8000-00805f9b34fb");
    public final static UUID UUID_C_HARDWARE = UUID.fromString("00002A27-0000-1000-8000-00805f9b34fb");
    public final static UUID UUID_C_SOFTWARE = UUID.fromString("00002A28-0000-1000-8000-00805f9b34fb");

    /*
     * BCS常量
     */
    public final static UUID UUID_SERVICE_BCS = UUID.fromString("46AC8170-71A9-4209-8F73-83B1622EE802");
    public final static UUID UUID_C_BCS_CP = UUID.fromString("46AC8171-71A9-4209-8F73-83B1622EE802");
    public final static UUID UUID_C_BCS_SAP = UUID.fromString("46AC8172-71A9-4209-8F73-83B1622EE802");
    public final static UUID UUID_C_BCS_ISDATA = UUID.fromString("46AC8173-71A9-4209-8F73-83B1622EE802");

    /**
     * Generic Access
     */
    public final static UUID UUID_SERVICE_GA = UUID.fromString("00001800-0000-1000-8000-00805f9b34fb");
    public final static UUID UUID_C_DEVICENAME_W = UUID.fromString("00002A00-0000-1000-8000-00805f9b34fb");
    public final static UUID UUID_C_APPEARENCE_R = UUID.fromString("00002A01-0000-1000-8000-00805f9b34fb");
    public final static UUID UUID_C_PAR_R = UUID.fromString("00002A04-0000-1000-8000-00805f9b34fb");

    /**
     * ImmediateAlart
     */
    public final static UUID UUID_SERVICE_ALART = UUID.fromString("00001802-0000-1000-8000-00805f9b34fb");
    public final static UUID UUID_C_ALART = UUID.fromString("00002A06-0000-1000-8000-00805f9b34fb");
    /**
     * 小米手环
     */
    //电池
    public static final UUID UUID_SERVICE_MILI = UUID.fromString("0000fee0-0000-1000-8000-00805f9b34fb");
    public static final UUID UUID_CHAR_BATTERY = UUID.fromString("0000ff0c-0000-1000-8000-00805f9b34fb");
    //控制,如震动等
    public static final UUID UUID_CHAR_CONTROL_POINT = UUID.fromString("0000ff05-0000-1000-8000-00805f9b34fb");
    public static final UUID UUID_CHAR_REALTIME_STEPS = UUID.fromString("0000ff06-0000-1000-8000-00805f9b34fb");

    public static final UUID UUID_CHAR_ACTIVITY = UUID.fromString("0000ff07-0000-1000-8000-00805f9b34fb");
    //通用通知
    public static final UUID UUID_CHAR_NOTIFICATION = UUID.fromString("0000ff03-0000-1000-8000-00805f9b34fb");
    //?
    public static final UUID UUID_FF10 = UUID.fromString("0000ff10-0000-1000-8000-00805f9b34fb");
    //用户信息
    public static final UUID UUID_CHAR_USER_INFO = UUID.fromString("0000ff04-0000-1000-8000-00805f9b34fb");
    public final static UUID UUID_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");// BluetoothGattDescriptor:Characteristic Client Config
    // ========================== 特性部分 ============================
    public static final UUID UUID_CHAR_DEVICE_INFO = UUID.fromString("0000ff01-0000-1000-8000-00805f9b34fb");
    public static final UUID UUID_CHAR_DEVICE_NAME = UUID.fromString("0000ff02-0000-1000-8000-00805f9b34fb");
    public static final UUID UUID_CHAR_DATA_TIME = UUID.fromString("0000ff0a-0000-1000-8000-00805f9b34fb");
    private static final UUID UUID_CHAR_LE_PARAMS = UUID.fromString("0000ff09-0000-1000-8000-00805f9b34fb");
    /**
     * 自检,读写
     */
    public static final UUID UUID_CHAR_TEST = UUID.fromString("0000ff0d-0000-1000-8000-00805f9b34fb");


    //QiCYCLE
    public final static UUID UUID_C_DI2 = UUID.fromString("0000fe73-0000-1000-8000-00805f9b34fb");// 功率特征

    public final static UUID UUID_SETTING_SERVICE = UUID.fromString("46AC8170-71A9-4209-8F73-83B1622EE802");// Ant设置服务
    public final static UUID UUID_C_SETTING = UUID.fromString("46AC8171-71A9-4209-8F73-83B1622EE802");// ANT设备DeviceNumber设置)
    public final static UUID UUID_PRODECT_SERIAL_NUMBER = UUID.fromString("46AC8172-71A9-4209-8F73-83B1622EE802");// 产品序列号读写)
    public final static UUID UUID_BIKE_SERIAL_NUMBER = UUID.fromString("46AC8174-71A9-4209-8F73-83B1622EE802");// 产品序列号读写)
    public final static UUID UUID_READ_ANT_SETTING = UUID.fromString("46AC8175-71A9-4209-8F73-83B1622EE802");// 读设置

    public final static UUID UUID_C_DI2_NEW = UUID.fromString("46AC8173-71A9-4209-8F73-83B1622EE802");// 读设置

}
