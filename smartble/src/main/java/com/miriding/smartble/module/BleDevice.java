package com.miriding.smartble.module;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.miriding.smartble.event.EventConnect;
import com.miriding.smartble.event.EventConnectOverTime;
import com.miriding.smartble.event.EventOnCharacteristicChanged;
import com.miriding.smartble.event.EventOnCharacteristicRead;
import com.miriding.smartble.event.EventOnDescriptorWrite;
import com.miriding.smartble.event.EventServiceDiscoverd;
import com.miriding.smartble.task.BleConnectTask;
import com.miriding.smartble.task.BleTask;

import de.greenrobot.event.EventBus;

/**
 * Created by kexuebiao on 16/4/27.
 */
public class BleDevice {
    final String TAG = "BleDevice";
    public String address;
    public String name = "";
    public BluetoothGatt gatt;
    public boolean autoConnect = true;

    public BleDevice(String address) {
        EventBus.getDefault().register(this);
        this.address = address;
    }

    /**
     * 连接监听
     *
     * @param event
     */
    public void onEvent(EventConnect event) {
        if (event.gatt.getDevice().getAddress().equals(address)) {
            if (!event.connected) {
                //连接断开,重新添加连接任务
                Log.i(TAG, "连接已断开,重新添加连接任务");
                OnConnectStateChange(false);
                autoConnect();
            }
        }
    }

    public void onEvent(EventOnCharacteristicRead event) {
        if (event.gatt.getDevice().getAddress().equals(address)) {
            OnConnectStateRead(event.characteristic);
        }
    }

    public void OnConnectStateRead(BluetoothGattCharacteristic ch) {

    }

    public void onEvent(EventOnCharacteristicChanged event) {
        if (event.gatt.getDevice().getAddress().equals(address)) {
            OnCharacteristicChanged(event.characteristic);
        }
    }

    public void OnCharacteristicChanged(BluetoothGattCharacteristic characteristic) {

    }

    public void OnConnectStateChange(boolean connected) {

    }

    public void onEvent(EventServiceDiscoverd event) {
        if (event.gatt.getDevice().getAddress().equals(address)) {
            if (event.success) {
                gatt = event.gatt;
                Log.i(TAG, "服务已连接");
                //服务已连接
                onServiceDiscoverd();
            } else {
                //服务已断开,重新添加连接任务
                Log.i(TAG, "服务已断开,重新添加连接任务");
                OnConnectStateChange(false);
                autoConnect();
            }
        }
    }

    public void onServiceDiscoverd() {
        OnConnectStateChange(true);
    }

    /**
     * 连接超时
     *
     * @param event
     */
    public void onEvent(EventConnectOverTime event) {
        if (event.device.address.equals(address)) {
            Log.e(TAG, "连接超时,重新执行任务");
            //连接超时,重新添加连接任务
            OnConnectStateChange(false);
            autoConnect();
        }
    }

    public void onNotify(EventOnDescriptorWrite event) {

    }

    public void onEvent(EventOnDescriptorWrite event) {
        if (event.gatt.getDevice().getAddress().equals(address)) {
            if (event.success) {
                //设备已断开,结束任务
                Log.i(TAG, " Notify 成功");
            } else {
                Log.e(TAG, " Notify 失败");
            }
            onNotify(event);
        }
    }

    public void start() {
        Log.i(TAG, "开始执行任务");
        BleBus.single.submitMission(new BleConnectTask(this).setAction("蓝牙连接 " + name));
    }

    public void close() {
        Log.i(TAG, "结束执行任务");
        if (gatt != null) {
            gatt.close();
            gatt = null;
        }
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof BleDevice) {
            return ((BleDevice) o).address.equals(address);
        }
        return super.equals(o);
    }

    void autoConnect() {
        if (autoConnect) {
            BleBus.single.submitMission(new BleConnectTask(this).setAction("蓝牙连接 " + name));
        }
    }


    void addBleTask(BleTask bleTask) {
        BleBus.single.submitMission(bleTask);
    }
}
