package com.miriding.smartble.module;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.miriding.smartble.task.BleNotifyTask;

import java.util.UUID;

/**
 * Created by kexuebiao on 16/5/6.
 */
public class Hr extends BleDevice {
    OnListen mOnListen;

    public void setOnListen(OnListen onListen) {
        mOnListen = onListen;
    }

    public Hr(String address) {
        super(address);
        this.name = "心率";
    }

    @Override
    public void onServiceDiscoverd() {
        BleBus.single.submitMission(new BleNotifyTask(this, Profile.UUID_SERVICE_HEARTRATE, Profile.UUID_C_HEARTRATE));
    }

    @Override
    public void OnCharacteristicChanged(BluetoothGattCharacteristic ch) {
        UUID uuid = ch.getUuid();
        byte[] data = ch.getValue();
        if (uuid.equals(Profile.UUID_C_HEARTRATE)) {
            if (data != null && data.length > 1) {
                int format;
                int flag = ch.getProperties();
                if ((flag & 0x01) != 0) {
                    format = BluetoothGattCharacteristic.FORMAT_UINT16;
                } else {
                    format = BluetoothGattCharacteristic.FORMAT_UINT8;
                }
                int hr = ch.getIntValue(format, 1);
                Log.i(TAG, "心率 = " + hr);
                if (mOnListen != null) {
                    mOnListen.OnHr(hr);
                }
            }
        }
    }

    public interface OnListen {
        void OnHr(int hr);

        void OnConnect(boolean connect);
    }
}
