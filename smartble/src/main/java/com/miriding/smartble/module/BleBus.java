package com.miriding.smartble.module;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.util.Log;

import com.miriding.smartble.event.EventConnect;
import com.miriding.smartble.event.EventOnCharacteristicChanged;
import com.miriding.smartble.event.EventOnCharacteristicRead;
import com.miriding.smartble.event.EventOnCharacteristicWrite;
import com.miriding.smartble.event.EventOnDescriptorWrite;
import com.miriding.smartble.event.EventServiceDiscoverd;
import com.miriding.smartble.task.BleTask;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;

/**
 * Created by kexuebiao on 16/4/26.
 */
public class BleBus extends BluetoothGattCallback {
    final String TAG = "BleBus";
    ExecutorService executor = Executors.newSingleThreadExecutor();
    public static Context context;
    public static BleBus single = new BleBus();


    public void submitMission(BleTask bleTask) {
        if (executor.isShutdown()) {
            Log.e(TAG, "重启任务");
            executor = Executors.newSingleThreadExecutor();
        }
        //提交任务
        executor.submit(bleTask);
    }

    public BluetoothGatt connectGatt(BleDevice bleDevice) {
        BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(bleDevice.address);
        return device.connectGatt(context, false, this);
    }

    public boolean discoverServices(BluetoothGatt gatt) {
        return gatt.discoverServices();
    }

    public void close() {
        if (!executor.isShutdown()) {
            //停止所有的任务
            executor.shutdown();
        }
        //关闭所有的设备连接
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        Log.e(TAG, "onConnectionStateChange  ");
        if (newState == BluetoothProfile.STATE_CONNECTED && status == BluetoothGatt.GATT_SUCCESS) {
            Log.e(TAG, "连接成功  " + gatt.getDevice().getAddress());
            EventBus.getDefault().post(new EventConnect(gatt, true));
        } else {
            Log.e(TAG, "连接失败  " + gatt.getDevice().getAddress());
            gatt.close();
            EventBus.getDefault().post(new EventConnect(gatt, false));
        }
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        if (status == BluetoothGatt.GATT_SUCCESS) {
            Log.e(TAG, "onServicesDiscovered-- 成功  " + gatt.getDevice().getAddress());
            EventBus.getDefault().post(new EventServiceDiscoverd(gatt, true));
        } else {
            Log.e(TAG, "onServicesDiscovered-- 失败  " + gatt.getDevice().getAddress());
            gatt.close();
            EventBus.getDefault().post(new EventServiceDiscoverd(gatt, false));
        }
    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        EventBus.getDefault().post(new EventOnCharacteristicChanged(gatt, characteristic));
    }

    @Override
    public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
        if (status == BluetoothGatt.GATT_SUCCESS) {
            Log.e(TAG, "onDescriptorWrite-- 成功  " + gatt.getDevice().getAddress());
            EventBus.getDefault().post(new EventOnDescriptorWrite(gatt, descriptor, true));
        } else {
            Log.e(TAG, "onDescriptorWrite-- 失败  " + gatt.getDevice().getAddress());
            EventBus.getDefault().post(new EventOnDescriptorWrite(gatt, descriptor, false));
        }
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        Log.e("XXX", "onCharacteristicRead");
        EventBus.getDefault().post(new EventOnCharacteristicRead(gatt, characteristic, status == BluetoothGatt.GATT_SUCCESS));
    }

    @Override
    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        Log.e("XXX", "onCharacteristicWrite");
        EventBus.getDefault().post(new EventOnCharacteristicWrite(gatt, characteristic, status == BluetoothGatt.GATT_SUCCESS));
    }

    /**
     * 读
     *
     * @param gatt
     * @param serviceUUID
     * @param characterUUID
     * @return
     */
    public boolean read(BluetoothGatt gatt, UUID serviceUUID, UUID characterUUID) {
        if (gatt != null) {
            BluetoothGattCharacteristic ch = getCharacteristic(gatt, serviceUUID, characterUUID);
            if (ch != null) {
                return gatt.readCharacteristic(ch);
            }
            Log.e(TAG, "读失败!");
        }
        return false;
    }

    /**
     * 写操作
     *
     * @param gatt
     * @param serviceUUID
     * @param characterUUID
     * @param data
     * @param withResponse
     * @return
     */
    public boolean write(BluetoothGatt gatt, UUID serviceUUID, UUID characterUUID, byte[] data, boolean withResponse) {
        if (gatt == null) {
            //gatt不存在
            return false;
        }
        BluetoothGattCharacteristic ch = getCharacteristic(gatt, serviceUUID, characterUUID);
        if (ch != null) {
            if (!withResponse) {
                //不需要response
                ch.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
            }
            ch.setValue(data);
            return gatt.writeCharacteristic(ch);
        }
        return false;
    }

    /**
     * notify
     *
     * @param serviceUUID
     * @param characterUUID
     * @return
     */
    public boolean Notify(BluetoothGatt gatt, UUID serviceUUID, UUID characterUUID) {
        Log.e(TAG, "Notify---------");
        if (gatt == null) {
            return false;
        }
        Log.e(TAG, "Notify");
        return notifyOrindicate(gatt, serviceUUID, characterUUID, false);
    }

    /**
     * indicate
     *
     * @param gatt
     * @param serviceUUID
     * @param characterUUID
     * @return
     */
    public boolean indicate(BluetoothGatt gatt, UUID serviceUUID, UUID characterUUID) {
        return notifyOrindicate(gatt, serviceUUID, characterUUID, true);
    }

    boolean notifyOrindicate(BluetoothGatt gatt, UUID serviceUUID, UUID characterUUID, boolean indicate) {
        BluetoothGattCharacteristic ch = getCharacteristic(gatt, serviceUUID, characterUUID);
        if (ch != null) {
            if (gatt.setCharacteristicNotification(ch, true)) {
                BluetoothGattDescriptor descriptor = ch.getDescriptor(Profile.UUID_CONFIG);
                if (descriptor != null) {
                    descriptor.setValue(!indicate ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                            : BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
                    return gatt.writeDescriptor(descriptor);
                }
            }
        }
        return false;
    }

    BluetoothGattCharacteristic getCharacteristic(BluetoothGatt gatt, UUID serviceUUID, UUID characterUUID) {
        if (gatt != null) {
            BluetoothGattService service = gatt.getService(serviceUUID);
            if (service != null) {
                BluetoothGattCharacteristic ch = service.getCharacteristic(characterUUID);
                if (ch != null) {
                    return ch;
                }
            }
        }
        return null;
    }
}
