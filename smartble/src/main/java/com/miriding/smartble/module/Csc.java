package com.miriding.smartble.module;


import android.bluetooth.BluetoothGattCharacteristic;

import com.miriding.smartble.Utils.BleTool;
import com.miriding.smartble.task.BleNotifyTask;

/**
 * Created by kexuebiao on 16/4/27.
 */
public class Csc extends BleDevice {
    public int wheelPerimeter = 1123;// mm
    private Long lastWheelRevolutions = null;
    private Integer lastWheelEventTime = null;
    private Integer lastCrankCount = null;
    private Integer lastCrankTime = null;
    OnListen mOnListen;

    public Csc(String address) {
        super(address);
        this.name = "CSC";
    }

    @Override
    public void onServiceDiscoverd() {
        BleBus.single.submitMission(new BleNotifyTask(this, Profile.UUID_SERVICE_CSC, Profile.UUID_C_CSC));
    }

    @Override
    public void OnCharacteristicChanged(BluetoothGattCharacteristic ch) {
        if (ch == null || ch.getValue() == null || ch.getValue().length == 0) {
            return;
        }
        Float Speed = null;
        Float Crank = null;

        int offset = 0;
        int flag = ch.getIntValue(
                BluetoothGattCharacteristic.FORMAT_UINT8, offset);
        offset++;

        Long CumulativeWheelRevolutions = null;
        Integer WheelEventTime = null;
        if ((flag & 0x01) != 0) {
            // 有轮子数据
            CumulativeWheelRevolutions = ch.getIntValue(
                    BluetoothGattCharacteristic.FORMAT_UINT32,
                    offset) & 0x00000000ffffffffL;
            offset += 4;

            WheelEventTime = ch.getIntValue(
                    BluetoothGattCharacteristic.FORMAT_UINT16,
                    offset);
            offset += 2;

            // 计算速度
            if (lastWheelRevolutions != null
                    && lastWheelEventTime != null
                    && (!WheelEventTime.equals(lastWheelEventTime))) {
                Speed = (BleTool.getRec(CumulativeWheelRevolutions,
                        lastWheelRevolutions) * (wheelPerimeter / 1000f))
                        / (BleTool.getRec(WheelEventTime, lastWheelEventTime) / 1024f)
                        * 3.6f;
            } else {
                Speed = 0f;
            }
            if (mOnListen != null)
                mOnListen.OnSpeed(Speed, WheelEventTime, CumulativeWheelRevolutions);

            lastWheelRevolutions = CumulativeWheelRevolutions;
            lastWheelEventTime = WheelEventTime;
        }

        Integer CumulativeCrankRevolutions = null;
        Integer CrankEventTime = null;
        if ((flag & 0x02) != 0) {
            // 有脚踏的数据
            CumulativeCrankRevolutions = ch.getIntValue(
                    BluetoothGattCharacteristic.FORMAT_UINT16,
                    offset);
            offset += 2;

            CrankEventTime = ch.getIntValue(
                    BluetoothGattCharacteristic.FORMAT_UINT16,
                    offset);
            offset += 2;

            boolean isRePeatData = false;
            if (lastCrankTime != null && CrankEventTime.intValue() == lastCrankTime.intValue()) {
                isRePeatData = true;
            }

            // 计算踏频
            if (lastCrankCount != null && lastCrankTime != null
                    && (CrankEventTime.intValue() != lastCrankTime.intValue())) {
                Crank = 1f * BleTool.getRec(CumulativeCrankRevolutions,
                        lastCrankCount)
                        / (BleTool.getRec(CrankEventTime, lastCrankTime) / 1024f / 60f);
            } else {
                Crank = 0f;
            }
            if (mOnListen != null)
                mOnListen.OnCrank(Crank, CrankEventTime, CumulativeCrankRevolutions, isRePeatData);

            lastCrankCount = CumulativeCrankRevolutions;
            lastCrankTime = CrankEventTime;
        }
    }

    public interface OnListen {
        void OnSpeed(float speed, int wheelTime, long wheelRevolution);

        void OnCrank(float crank, int crankTime, int crankRevolution, boolean isRePeatData);

        void OnConnect(boolean connect);

        void OnBleClosed();
    }
}
