package com.miriding.smartisan;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TranActivity extends Activity {
    String recordTimeStr;
    double record_time;
    long errorCounts = 0;
    long i = 0;
    @Bind(R.id.tv_result)
    TextView tvResult;
    @Bind(R.id.btn_start)
    Button btnStart;

    boolean goon = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        tvResult.setText("---");

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (goon) {
                    i++;
                    recordTimeStr = "2016-05-16 07:33:40";
                    record_time = (double) (parseDate(recordTimeStr).getTime() / 1000);//时间戳（秒）
                    if (Math.abs(record_time - 1463355220.0d) > 1) {
                        Log.e("XXX", i + "_错误_" + record_time);
                        errorCounts++;
                    }
                    if (i % 200 == 100) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvResult.setText(i + "_错误_" + errorCounts);
                            }
                        });
                    }
                    Log.e("XXX", i + "_错误_" + errorCounts);
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (goon) {
                    i++;
                    recordTimeStr = "2012-03-12 17:23:30";
                    record_time = (double) (parseDate(recordTimeStr).getTime() / 1000);//时间戳（秒）
                    if (Math.abs(record_time - 1331508820.0d) > 1) {
                        errorCounts++;
                    }
                    if (i % 200 == 0) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvResult.setText(i + "_错误_" + errorCounts);
                            }
                        });
                    }
                    Log.e("XXX", i + "_错误_" + errorCounts);
                }
            }
        }).start();
    }


    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);

    public static Date parseDate(String dateString) {
        if (dateString == null) {
            return null;
        }
        if (sdf == null) {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
        }
        try {
            return sdf.parse((dateString));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    @OnClick(R.id.btn_start)
    public void onClick() {
        goon = false;
    }
}
