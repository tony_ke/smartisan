package com.miriding.smartisan;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.miriding.smartble.module.BleBus;
import com.miriding.smartble.module.Csc;
import com.miriding.smartble.module.Hr;
import com.miriding.smartble.module.R1;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    String TAG = "Main2Activity";
    R1 r1;
    Hr hr;
    Csc csc;
    String cscAddress = "FD:48:A3:59:AC:CF";
    String r1Address = "E5:88:C9:6D:88:36";
    String hrAddresss = "C2:A5:33:06:2E:0A";
    @Bind(R.id.btn_start)
    Button btnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        BleBus.context = this;
        r1 = new R1(r1Address);
        r1.setOnListen(mOnlisten);
    }

    R1.OnListen mOnlisten = new R1.OnListen() {

        @Override
        public void OnBattery(int battery) {
            Log.e(TAG, "电池  = " + battery);
        }

        @Override
        public void OnDi2(int front, int rear, int battery, int power_battery) {
            Log.e(TAG, "电变  = " + front + "-" + rear + "- 电变电池" + battery + "- 功率电池 " + power_battery);
        }

        @Override
        public void OnPower(float power, float speed, float cadence, float distance) {
            Log.e(TAG, "功率  = " + power + "- 速度 = " + speed + " - 踏频 = " + cadence + " - 距离 =" + distance);
        }

        @Override
        public void OnConnect(boolean connect) {
            Log.d(TAG, "连接状态 = " + connect);
        }

        @Override
        public void OnBleClosed() {
            Log.d(TAG, "OnBleClosed");
        }

        @Override
        public void OnAntScanResult(byte[] data) {

        }

        @Override
        public void OnReadAntSetting(byte[] antPowerNumber, byte[] antDi2Number) {

        }

        @Override
        public void OnReadProductSN(String number) {

        }

        @Override
        public void OnReadBikeSN(String number) {

        }
    };

    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy");
        r1.close();
        BleBus.single.close();
        super.onDestroy();
    }

    @OnClick({R.id.btn_start, R.id.btn_write})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_start:
                r1.start();
                break;
            case R.id.btn_write:
                r1.writeBikeSN("11111/22222123");
                r1.readBikeSN();
                break;
        }
    }
}
