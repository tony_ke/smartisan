package com.miriding.smartisan;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Button;

import com.LocService;
import com.baidu.location.BDLocation;

import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

public class LocActivity extends Activity {
    Subscription sb;
    @Bind(R.id.btn_start)
    Button btnStart;
    @Bind(R.id.btn_write)
    Button btnWrite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Log.e("KKK", "onStart");

        sb = Observable.timer(10, TimeUnit.SECONDS)
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {

                    }
                });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @OnClick(R.id.btn_start)
    public void onClick() {
        Log.e("KKK", "onClick");
        Intent bindIntent = new Intent(LocActivity.this, LocService.class);
        bindService(bindIntent, sconnection, Context.BIND_AUTO_CREATE);
    }

    private LocService locService;
    ServiceConnection sconnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            locService = ((LocService.LocBinder) service).getService();
            locService.setOnLocation(new LocService.LocListener() {
                @Override
                public void OnResult(BDLocation location,String city) {
                    Log.e("KKK", "location = " + location.getLatitude());
                }
            });
        }

        public void onServiceDisconnected(ComponentName name) {

        }
    };
}
