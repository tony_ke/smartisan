package com;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;

/**
 * Created by kexuebiao on 16/5/17.
 */
public class LocService extends Service implements BDLocationListener {
    public static String city;
    private LocationClient mLocationClient;
    private int locTime = 1001;
    private long lastGpsLocTime = 0l;
    LocBinder locBinder = new LocBinder();
    LocListener mLocListener;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("KKK", "开启城市获取");
        mLocationClient = new LocationClient(this);
        mLocationClient.setLocOption(getDefaultLocationClientOption(true));
        mLocationClient.registerLocationListener(this);
        mLocationClient.start();
    }

    LocationClientOption getDefaultLocationClientOption(boolean needAddress) {
        Log.e("KKK", "开启城市获取");
        lastGpsLocTime = 0l;
        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(true); // 打开gps
        //已定位城市只采gps数据,未定位城市的采用高精度定位（包括网络定位和GPS定位）
        if (needAddress) {
            Log.e("KKK", "需要城市");
            option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);//设置定位模式
            option.setIsNeedAddress(true);//返回的定位结果包含地址信息
        } else {
            Log.e("KKK", "不需要城市");
            option.setLocationMode(LocationClientOption.LocationMode.Device_Sensors);//设置定位模式
            option.setIsNeedAddress(false);//返回的定位结果包含地址信息
        }
        option.setScanSpan(locTime);// 设置定位模式，小于1秒则一次定位;大于等于1秒则定时定位
        option.setCoorType("gcj02"); // 设置坐标类型
        option.setNeedDeviceDirect(false);//返回的定位结果包含手机机头的方向
        return option;
    }


    @Override
    public void onReceiveLocation(BDLocation bdLocation) {
        if (bdLocation != null && mLocListener != null) {
            mLocListener.OnResult(bdLocation,city);
            if (bdLocation.getCity() != null) {
                city = bdLocation.getCity();
                Log.e("KKK", "city  =" + city);
                //停止获取城市
                mLocationClient.setLocOption(getDefaultLocationClientOption(false));
            }
        }
    }

    @Override
    public void onDestroy() {
        if (mLocationClient != null && mLocationClient.isStarted()) {
            Log.i("cmh", "location stop");
            mLocationClient.stop();
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return locBinder;
    }

    public class LocBinder extends Binder {
        public LocService getService() {
            return LocService.this;
        }
    }

    public void setOnLocation(LocListener mLocListener) {
        this.mLocListener = mLocListener;
    }

    public interface LocListener {
        void OnResult(BDLocation location,String city);
    }
}
